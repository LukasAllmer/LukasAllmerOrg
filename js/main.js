// Getting window size and loading correct background image
adjustBackgroundToWindowSize();
window.addEventListener("resize", adjustBackgroundToWindowSize);

function adjustBackgroundToWindowSize(){
    let width = window.innerWidth;
    let height = window.innerHeight;
    let html = document.getElementsByClassName("pageBackground")[0];

    if (height <= 720 && width <= 1280) {
        html.style.backgroundImage = "url('/img/background_1280x720.webp')";
    } else if (height <= 900 && width <= 1600) {
        html.style.backgroundImage = "url('/img/background_1600x900.webp')";
    } else if (height <= 1080 && width <= 1920) {
        html.style.backgroundImage = "url('/img/background_1920x1080.webp')";
    } else if (height <= 1440 && width <= 2560) {
        html.style.backgroundImage = "url('/img/background_2560x1440.webp')";
    } else if (height <= 2160 && width <= 3840) {
        html.style.backgroundImage = "url('/img/background_3840x2160.webp')";
    } else {
        html.style.backgroundImage = "url('/img/background_6070x3414.webp')";
    }  
}